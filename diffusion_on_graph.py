import numpy
import copy
import scipy.sparse, scipy.sparse.linalg
import scipy.linalg, scipy.integrate
import networkx as nx
from PIL import Image
from skeleton_to_graph import return_graph
from skimage import morphology
import matplotlib.pyplot as plt
from numba import jit

@jit
def power_iteration(A: numpy.ndarray, num_iterations: int):
    b_k = numpy.random.rand(A.shape[1])

    for _ in range(num_iterations):
        b_k1 = numpy.dot(A, b_k)
        b_k1_norm = numpy.linalg.norm(b_k1)
        b_k = b_k1 / b_k1_norm

    l_k = numpy.dot(b_k.transpose(), numpy.dot(A, b_k))
    return (l_k, b_k)


def eigenvalues(graph: nx.Graph):
    n = graph.number_of_nodes()

    L_matr = nx.laplacian_matrix(graph)
    L_matr = scipy.sparse.csr_matrix.toarray(L_matr)

    # We know that the smallest eigenvalue is 0
    w = numpy.zeros(n)
    v = numpy.zeros((n,n))
    w[n - 1] = 0
    v[n - 1] = (1/numpy.sqrt(n)) * numpy.ones(n)

    # Finding the rest of eigen values
    L1 = copy.deepcopy(L_matr)
    for i in range(n - 1):
        w_i, v_i = power_iteration(L1, 50)
        w[i] = w_i
        v[i] = v_i
        L1 = L1 - w_i * numpy.outer(v_i, v_i.transpose())
    return (w, v)


def def_func(t, n):
    return numpy.zeros(n)


def graph_diffusion_t(graph: nx.Graph, u: numpy.ndarray, 
                      t: float, c: float, func):
    n = graph.number_of_nodes()
    w, v = eigenvalues(graph)
    a_0 = [numpy.dot(u, v_i)/(numpy.linalg.norm(v_i))**2 for v_i in v]

    a_t = numpy.zeros(n)
    for i in range(n):
        fv = lambda t: (numpy.dot(func(t, n), v[i]) * numpy.exp(c*w[i]*t))
        a_t[i] = a_0[i] + scipy.integrate.quad(fv, 0, t)[0]

    u = numpy.sum([a_t[i] * v[i] * numpy.exp(-c*w[i]*t) for i in range(n)], axis=0)
    return u


def graph_diffusion(graph: nx.Graph, u: numpy.ndarray, 
                  t: float, dt: float, c: float,
                  func=def_func, 
                  return_ut=False, delta_t=0):

    if return_ut:
        if delta_t == 0:
            print('Please set delta_t!')
            delta_t = t/10

        N = int(t/dt)
        M = int(t/delta_t)
        n = graph.number_of_nodes()
        u_t = numpy.zeros((M, n))
        u_t[0] = u

        w, v = eigenvalues(graph)
        a_0 = [numpy.dot(u, v_i)/(numpy.linalg.norm(v_i))**2 for v_i in v]
        a_t = a_0

        fv = [[numpy.dot(func(i*dt, n), v[k])* numpy.exp(c*w[k]*i*dt) for i in range(N)] for k in range(n)]

        for i in range(1, N):
            for k in range(n):
                a_t[k] += fv[k][i]*dt
            u = numpy.sum([a_t[k] * v[k] * numpy.exp(-c*w[k]*i*dt) for k in range(n)], axis=0)
            if (i*dt) % delta_t == 0:
                u_t[int((i*dt) // delta_t)] = u
        return u_t
    else:
        return graph_diffusion_t(graph, u, t, c, func)
    

def ut_at_node(u_t: numpy.ndarray, i):
    N = u_t.shape[0]
    node_t = numpy.asarray([u_t[k][i] for k in range(N)])
    return node_t


if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    c = 1.
    t = 800.
    dt = 1
    delta_t = 1
    N = int(t/dt)
    M = int(t/delta_t)

    result = morphology.skeletonize(f)
    graph = return_graph(result, weight_type="length")
    n = graph.number_of_nodes()

    def const_func(t, n):
        f = numpy.zeros(n)
        f[0] = 1.
        return f
    
    def osc(t, n):
        f = numpy.zeros(n)
        f[0] = numpy.cos(0.01 * t * numpy.pi)
        return f

    u_0 = numpy.zeros(n, dtype=float)
    u_0[0] = 10.
    u_t = graph_diffusion(graph, u_0, t, dt, c, func=osc, return_ut=True, delta_t=delta_t)
    print(sum(u_t[-1])) # conservation of energy


    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(f, cmap='gray')
    pos = nx.get_node_attributes(graph,'pos')
    nx.draw_networkx(graph, pos, with_labels=True, node_size=100, node_color=u_t[-1], edge_color='gray', ax=ax1)

    x = numpy.linspace(0, t, M)
    y1 = ut_at_node(u_t, 0)
    y2 = ut_at_node(u_t, 3)
    y3 = ut_at_node(u_t, 5)
    ax2.plot(x, y1, label='node 0')
    ax2.plot(x, y2, label='node 3')
    ax2.plot(x, y3, label='node 5')
    ax2.legend()
    plt.show()