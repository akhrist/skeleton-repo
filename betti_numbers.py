import numpy
import networkx as nx
from PIL import Image
import skimage.measure
from skimage import morphology
from skeleton_to_graph import return_graph

def add_border(img: numpy.ndarray):
    y, x = img.shape
    new_img = numpy.zeros((y+2, x+2))
    for j in range(y):
        for i in range(x):
            new_img[j+1][i+1] = img[j][i]
    return new_img


def connected_components(img: numpy.ndarray, type=8, value=1):
    mask = numpy.zeros(img.shape)
    y, x = img.shape
    for j in range(y):
        for i in range(x):
            if img[j][i] == value:
                mask[j][i] = 1
    c = 2
    if type == 4:
        c = 1

    count = skimage.measure.label(mask, connectivity=c, return_num=True)[1]
    return count


def betti_img(img: numpy.ndarray):
    b0 = connected_components(img, type=8, value=1)
    b1 = connected_components(img, type=4, value=0) - 1
    return (b0, b1)


def betti_graph(g: nx.Graph):
    b0 = nx.algorithms.components.number_connected_components(g)
    b1 = g.number_of_edges() - g.number_of_nodes() + b0
    return (b0, b1)


if __name__ == "__main__":
    f = Image.open('Images/plenka_3.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255
    f = add_border(f)

    result = morphology.skeletonize(f)
    graph = return_graph(result, weight_type='ones')

    print(f'b0, b1 of image: {betti_img(f)}')
    print(f'b0, b1 of graph: {betti_graph(graph)}')