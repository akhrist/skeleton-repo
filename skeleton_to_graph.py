import sknw
import numpy
import pyvis as pv
import networkx as nx
from PIL import Image
from skimage import morphology
import matplotlib.pyplot as plt


def return_graph(result, weight_type, c=0, dt=0):
    graph = sknw.build_sknw(result, multi=True, ring=True)
    graph2 = nx.Graph()

    for n in graph.nodes():
        x0, y0 = graph.nodes()[n]['o']
        graph2.add_node(n, pos=(y0, x0))

    lmax = 0
    for f, s in graph.edges():
        p = graph[f][s]
        l = len(p[0]['pts'])
        if lmax < l:
            lmax = l

    to_ignore = []
    for f, s in graph.edges():
        p = graph[f][s]
        l = len(p[0]['pts'])

        # if edge is loop
        if f == s and l > 3: # ignore little loops
            if weight_type == 'length':
                w = 3/l
            elif weight_type == 'ones':
                w = 1
            elif weight_type == 'exponent':
                t = (l/3)/lmax
                w = numpy.exp(-t)
            else:
                print("Unnkown weight type")

            n = n + 1
            x0, y0 = p[0]['pts'][l//3]
            graph2.add_node(n, pos=(y0, x0))
            graph2.add_edge(f, n, weight=w)

            n = n + 1
            x0, y0 = p[0]['pts'][2*l//3]
            graph2.add_node(n, pos=(y0, x0))
            graph2.add_edge(f, n, weight=w)

            graph2.add_edge(n-1, n, weight=w)


        # if parallel edges
        if len(p) > 1 and [f, s] not in to_ignore:
            for i in range(len(p)):
                l = len(p[i]['pts'])
                x0, y0 = p[i]['pts'][l//2]
                n = n + 1
                graph2.add_node(n, pos=(y0, x0))

                if weight_type == 'length':
                    w = 2/l
                elif weight_type == 'ones':
                    w = 1
                elif weight_type == 'exponent':
                    t = (l/2)/lmax
                    w = numpy.exp(-t)
                else:
                    print("Unnkown weight type")

                graph2.add_edge(f, n, weight=w)
                graph2.add_edge(s, n, weight=w)
            to_ignore.append([f,s])


    for f, s in graph.edges():
        p = graph[f][s]
        l = float(len(p[0]['pts']))

        if weight_type == 'length':
            w = 1/l
        elif weight_type == 'ones':
            w = 1
        elif weight_type == 'exponent':
            t = l/lmax
            w = numpy.exp(-t)
        else:
            print("Unnkown weight type")

        if f != s and [f,s] not in to_ignore:
            graph2.add_edge(f, s, weight=w)

    return graph2


if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    result = morphology.skeletonize(f)
    plt.imshow(f/2 + result, cmap='gray')

    graph = return_graph(result, weight_type='ones')
    pos = nx.get_node_attributes(graph,'pos')

    nx.draw_networkx(graph, pos, with_labels=True, node_size=100, node_color='#3333b2', edge_color='b')
    plt.show()