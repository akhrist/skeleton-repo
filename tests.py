import sknw
import pyvis as pv
import networkx as nx
import skan
from skan import draw, skeleton_to_csgraph
from skimage import io, morphology
from PIL import Image
import matplotlib.pyplot as plt
import numpy


def SKNW(result):
    # sknw
    graph = sknw.build_sknw(result, multi=True)

    plt.imshow(f, cmap='gray')

    # draw edges by pts
    for (s,e) in graph.edges():
        p = graph[s][e]
        for i in range(len(p)):
            ps = p[i]['pts']
            plt.plot(ps[:,1], ps[:,0], 'green')
        
    # draw node by o
    nodes = graph.nodes()
    ps = numpy.array([nodes[i]['o'] for i in nodes])
    plt.plot(ps[:,1], ps[:,0], 'r.')

    plt.title('Build Graph')
    plt.show()

    return graph


def SKAN(result):
    pixel_graph, coordinates = skeleton_to_csgraph(result)

    draw.overlay_skeleton_networkx(pixel_graph, coordinates)
    plt.show()


if __name__ == "__main__":
    f = Image.open('Images/Test_12.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    result = morphology.skeletonize(f)

    graph = SKNW(result)

    graph2 = nx.MultiGraph()

    for n in graph.nodes():
        coords = graph.nodes()[n]['o']
        graph2.add_node(n) #, y=10*coords[0], x=10*coords[1])

    for f, s in graph.edges():
        p = graph[f][s]
        for i in range(len(p)):
            ps = p[i]['pts']
            graph2.add_edge(f, s, length=len(ps))

    net = pv.network.Network(height='100%', width='60%', bgcolor='#222222', font_color='white')
    net.from_nx(graph2)

    net.toggle_physics(False)

    net.show('mygraph.html')
    
    # SKAN(result)