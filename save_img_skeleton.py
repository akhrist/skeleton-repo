from PIL import Image
import numpy
from skimage import morphology

if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    result = morphology.skeletonize(f)
    result = numpy.asarray(result, dtype=int)

    i = f + result
    i = i * 255//2
    i = numpy.asarray(i, dtype='uint8')

    img = Image.fromarray(i, mode='L')
    img.save("skeleton.png")

