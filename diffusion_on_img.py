import numpy
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
from numba import jit
from math import ceil


class Pixel:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


@jit
def timestep(f: numpy.ndarray, u0: numpy.ndarray, dt: float, c: float):
    u = numpy.zeros(u0.shape)
    y, x = u0.shape
    
    for j in range(1, y-1):
        for i in range(1, x-1):
            if f[j][i] == 1:
                if f[j][i+1] != 0 and f[j][i-1] != 0:
                    Lxu = u0[j][i+1] - 2*u0[j][i] + u0[j][i-1]
                elif f[j][i+1] == 0 and f[j][i-1] != 0:
                    Lxu = - u0[j][i] + u0[j][i-1]
                elif f[j][i+1] != 0 and f[j][i-1] == 0:
                    Lxu = u0[j][i+1] - u0[j][i]
                else:
                    Lxu = 0


                if f[j+1][i] != 0 and f[j-1][i] != 0:
                    Lyu = u0[j+1][i] - 2*u0[j][i] + u0[j-1][i]
                elif f[j+1][i] == 0 and f[j-1][i] != 0:
                    Lyu = - u0[j][i] + u0[j-1][i]
                elif f[j+1][i] != 0 and f[j-1][i] == 0:
                    Lyu = u0[j+1][i] - u0[j][i]
                else:
                    Lyu = 0
                
                u[j][i] = u0[j][i] + c * dt * (Lxu + Lyu)
    return u


def def_func(x, y, t):
    f = numpy.zeros((y,x))
    return f


def img_diffusion(img: numpy.ndarray, u: numpy.ndarray,
                  t: float, dt: float, c: float,
                  func=def_func,
                  return_ut=False, delta_t=0):
    y, x = u.shape
    N = int(t/dt)

    print(f'Numerically stable: {bool(dt < 1/(4*c))}')

    if return_ut:
        if delta_t == 0:
            delta_t = dt
        M = ceil(t/delta_t)
        u_t = numpy.zeros((M, y, x))
        u_t[0] = u

    for i in range(1, N):
        u = timestep(img, u, dt, c)
        u = u + func(x, y, (i-1)*dt)

        if return_ut:
            if (i*dt) % delta_t == 0:
                u_t[int((i*dt) // delta_t)] = u
    
    print('Image diffusion done')
    if return_ut == True:
        return u_t
    else:
        return u
    

def ut_at_coords(u_t: numpy.ndarray, i, j):
    M = u_t.shape[0]
    pos_t = numpy.asarray([u_t[k][j][i] for k in range(M)])
    return pos_t


def area(img: numpy.ndarray):
    y, x = img.shape
    S = 0
    for j in range(y):
        for i in range(y):
            if img[j][i]:
                S += 1
    return S


if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=float)
    f = f//255
    y, x = f.shape

    c = 10.
    t = 500.
    dt = 0.01
    delta_t = 5.
    N = int(t/dt)
    M = int(t/delta_t)

    u0 = numpy.zeros(f.shape)
    u0[37][182] = 0.

    pixel1 = Pixel(182, 37)
    pixel2 = Pixel(70, 80)
    pixel3 = Pixel(130, 80)
    pixels = [pixel1, pixel2, pixel3]

    def const_func(x, y, t):
        f = numpy.zeros((y,x))
        f[pixel1.y][pixel1.x] = 0.1
        return f
    
    def osc1(x, y, t):
        f = numpy.zeros((y,x))
        f[pixel1.y][pixel1.x] = numpy.cos(0.01 * t * numpy.pi)
        return f
    
    def osc2(x, y, t):
        f = numpy.zeros((y,x))
        f[pixel1.y][pixel1.x] = numpy.cos(0.01 * t)
        f[pixel2.y][pixel2.x] = numpy.sin(0.05 * t)
        return f
    
    u_t = img_diffusion(f, u0, t, dt, c, func=osc1, return_ut=True, delta_t=delta_t)
    print(sum(sum(u_t[-1])))

    def plotheatmap(u_k, k):
        plt.clf() # Clear the current plot figure
        plt.title(f"Temperature at t = {k*delta_t:.3f} unit time")
        plt.xlabel("x")
        plt.ylabel("y")
        
        plt.pcolormesh(u_k, vmax=1, vmin=-1)
        plt.colorbar()
        
        return plt
    
    def animate(k):
        plotheatmap(u_t[k], k)

    #anim = animation.FuncAnimation(plt.figure(), animate, interval=1, frames=(M), repeat=False)
    #anim.save("solution.gif")
    #print('Done')

    u1 = ut_at_coords(u_t, pixel1.x, pixel1.y)
    u2 = ut_at_coords(u_t, pixel2.x, pixel2.y)
    u3 = ut_at_coords(u_t, pixel3.x, pixel3.y)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(u_t[-1])
    ax1.plot(pixel1.x, pixel1.y, 'o')
    ax1.plot(pixel2.x, pixel2.y, 'o')
    ax1.plot(pixel3.x, pixel3.y, 'o')

    x = numpy.linspace(0, t, M)
    ax2.plot(x, u1, label='pt 1')
    ax2.plot(x, u2, label='pt 2')
    ax2.plot(x, u3, label='pt 3')
    ax2.legend()
    plt.show()