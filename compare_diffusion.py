import numpy
import networkx as nx
from skimage import morphology
from PIL import Image
import matplotlib.pyplot as plt

from skeleton_to_graph import return_graph
from diffusion_on_graph import graph_diffusion, ut_at_node
from diffusion_on_img import img_diffusion, ut_at_coords, area

from scipy import sparse
from ripser import ripser
from persim import plot_diagrams

def node_coords(pos_dict, k):
    x, y = pos_dict[k]
    return int(x), int(y)

def persist_diag(y):
    N = len(y)
    I = numpy.arange(N-1)
    J = numpy.arange(1, N)
    V = numpy.maximum(y[0:-1], y[1::])

    I = numpy.concatenate((I, numpy.arange(N)))
    J = numpy.concatenate((J, numpy.arange(N)))
    V = numpy.concatenate((V, y))
    D = sparse.coo_matrix((V, (I, J)), shape=(N, N)).tocsr()
    diagram = ripser(D, distance_matrix=True)['dgms'][0]
    return diagram

if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    # INIT PARAMS
    c1 = 10.
    c2 = 1.
    t1 = 1000.
    t2 = 100.
    dt1 = 0.001
    dt2 = 1
    delta_t1 = 5.
    delta_t2 = 1.
    N1 = int(t1/dt1)
    N2 = int(t2/dt2)
    M1 = int(t1/delta_t1)
    M2 = int(t2/delta_t2)
    S = area(f)

    result = morphology.skeletonize(f)
    graph = return_graph(result, weight_type="length")
    n = graph.number_of_nodes()
    pos = nx.get_node_attributes(graph,'pos')

    # DIFFUSION ON IMAGE
    u01 = numpy.zeros(f.shape, dtype=float)
    x0, y0 = node_coords(pos, 0)
    u01[y0][x0] = 0.

    def i_osc(x, y, t):
        f = numpy.zeros((y,x))
        f[y0][x0] = numpy.cos(0.01 * t * numpy.pi)
        return f
    
    u1 = img_diffusion(f, u01, t1, dt1, c1, func=i_osc, return_ut=True, delta_t=delta_t1)

    # DIFFUSION ON GRAPH
    def g_osc(t, n):
        f = numpy.zeros(n)
        f[0] = numpy.cos(0.1 * t * numpy.pi)
        return f
    
    u02 = numpy.zeros(n, dtype=float)
    u02[0] = 0.
    u2 = graph_diffusion(graph, u02, t2, dt2, c2, func=g_osc, return_ut=True, delta_t=delta_t2)
    
    # PLOTTING
    x01, y01 = node_coords(pos, 3)
    x02, y02 = node_coords(pos, 5)

    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    ax1.imshow(u1[-1] + f*u1[-1].max()*0.1)
    ax1.plot(x0, y0, 'o')
    ax1.plot(x01, y01, 'o')
    ax1.plot(x02, y02, 'o')

    ax2.imshow(f, cmap='gray')
    nx.draw_networkx(graph, pos, with_labels=True, node_size=100, node_color=u2[-1], edge_color='gray', ax=ax2)
    plt.show()

    fig = plt.figure()
    ax3 = fig.add_subplot(221)
    ax4 = fig.add_subplot(222)
    ax5 = fig.add_subplot(223)
    ax6 = fig.add_subplot(224)

    # IMAGE PLOTS
    x = numpy.linspace(0, t1, M1)
    y1 = ut_at_coords(u1, x0, y0)
    y2 = ut_at_coords(u1, x01, y01)
    y3 = ut_at_coords(u1, x02, y02)
    ax3.plot(x, y1, label='pt 1')
    ax3.plot(x, y2, label='pt 2')
    ax3.plot(x, y3, label='pt 3')
    #ax3.set_ylim(bottom=-0.1, top=n)
    ax3.legend()

    # GRAPH PLOTS
    x = numpy.linspace(0, t2, M2)
    z1 = ut_at_node(u2, 0)
    z2 = ut_at_node(u2, 3)
    z3 = ut_at_node(u2, 5)
    ax4.plot(x, z1, label='node 0')
    ax4.plot(x, z2, label='node 3')
    ax4.plot(x, z3, label='node 5')
    ax4.legend()

    # PERSISTENCE DIAGRAMS
    d1 = persist_diag(y1)
    d2 = persist_diag(y2)
    d3 = persist_diag(y3)
    plt.sca(ax5)
    plot_diagrams([d1, d2, d3], show=False, labels=['pt 1', 'pt 2', 'pt 3'])

    d1 = persist_diag(z1)
    d2 = persist_diag(z2)
    d3 = persist_diag(z3)
    plt.sca(ax6)
    plot_diagrams([d1, d2, d3], show=False, labels=['node 0', 'node 3', 'node 5'])
    plt.show()