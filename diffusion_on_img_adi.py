import numpy
from PIL import Image
from numba import jit

def neighbors8(img, i, j):
    P = numpy.zeros(8)
    P[0] = img[j-1][i]
    P[1] = img[j][i+1]
    P[2] = img[j+1][i]
    P[3] = img[j][i-1]
    P[4] = img[j-1][i-1]
    P[5] = img[j-1][i+1]
    P[6] = img[j+1][i-1]
    P[7] = img[j+1][i+1]
    return P


def has_black_neighbors(img, i, j):
    P = neighbors8(img, i, j)
    if not all(P):
        return True
    else:
        return False


def find_edges(img: numpy.ndarray):
    y, x = img.shape
    edge = numpy.zeros(img.shape)
    for j in range(y):
        for i in range(x):
            if img[j][i] == 1 and has_black_neighbors(img, i, j):
                edge[j][i] = 1
    return edge

@jit
def TDMA(a,b,c,d):
    # a_i x_i-1 + b_i x_i + c_i x_i+1 = d_i
    n = len(d)
    w = numpy.zeros(n-1,float)
    g = numpy.zeros(n, float)
    p = numpy.zeros(n,float)
    
    w[0] = c[0]/b[0]
    g[0] = d[0]/b[0]

    for i in range(1,n-1):
        w[i] = c[i]/(b[i] - a[i-1]*w[i-1])
    for i in range(1,n):
        g[i] = (d[i] - a[i-1]*g[i-1])/(b[i] - a[i-1]*w[i-1])
    p[n-1] = g[n-1]
    for i in range(n-1,0,-1):
        p[i-1] = g[i-1] - w[i-1]*p[i]
    return p


def adi_timestep(f: numpy.ndarray, u0: numpy.ndarray, dt: float, c: float):
    u = numpy.zeros(u0.shape)
    y, x = u0.shape
    edges = find_edges(f)

    line = 0
    for j in range(1, y-1):
        for i in range(1, x-1):
            if edges[j][i] == 1 and line == 0: # line begins
                line = 1
            elif edges[j][i] == 1 and line == 1: # line ends
                line = 0
            while line == 1:
                pass
    return u

if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=float)
    f = f//255
    y, x = f.shape

    