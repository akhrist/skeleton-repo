from PIL import Image
import numpy
import pygimli as pg
import pygimli.meshtools as mt
from math import floor

def neighbors8(img, i, j):
    P = numpy.zeros(8)
    P[0] = img[j-1][i]
    P[1] = img[j][i+1]
    P[2] = img[j+1][i]
    P[3] = img[j][i-1]
    P[4] = img[j-1][i-1]
    P[5] = img[j-1][i+1]
    P[6] = img[j+1][i-1]
    P[7] = img[j+1][i+1]
    return P


def has_black_neighbors(img, i, j):
    P = neighbors8(img, i, j)
    if not all(P):
        return True
    else:
        return False


def find_first_pt(img):
    y, x = img.shape
    for j in range(y):
        for i in range(x):
            if img[j][i] == 1:
                return (i, j)
    return (-1, -1)


def find_next_edge_pt(img, i, j):
    P = neighbors8(img, i, j)
    if P[0] == 1:
        return [j-1,i]
    elif P[1] == 1:
        return [j,i+1]
    elif P[2] == 1:
        return [j+1,i]
    elif P[3] == 1:
        return [j,i-1]
    elif P[4] == 1:
        return [j-1,i-1]
    elif P[5] == 1:
        return [j-1,i+1]
    elif P[6] == 1:
        return [j+1,i-1]
    elif P[7] == 1:
        return [j+1,i+1]
    else:
        return [-1, -1]
    

def find_edges(img: numpy.ndarray):
    y, x = img.shape
    edge = numpy.zeros(img.shape)
    for j in range(y):
        for i in range(x):
            if img[j][i] == 1 and has_black_neighbors(img, i, j):
                edge[j][i] = 1
    return edge
    

def img_diffusion_pg(img: numpy.ndarray, u0, c_dict, times,
                     func=0,
                     show_geometry=True, show_mesh=True,
                     show_solution=True, axe=None):
    y, x = img.shape
    edge = find_edges(img)

    boundaries = []
    while edge.any():
        boundary = []
        x0, y0 = find_first_pt(edge)

        while x0 != -1:
            edge[y0][x0] = 0
            boundary.append([x0,y0])
            y0, x0 = find_next_edge_pt(edge, x0, y0)

        boundaries.append(boundary)

    geom = mt.createWorld([0,0], [x,y], worldMarker=True)
    for j in range(len(boundaries)):
        boundary = boundaries[j]
        poly = mt.createPolygon(boundary, isClosed=True, marker=j+1, boundaryMarker=j+1)
        geom = geom + poly
    if show_geometry:
        pg.show(geom, markers=True, boundaryMarker=True)


    mesh = mt.createMesh(geom)

    if show_mesh:
        pg.show(mesh, showMesh=True)
        #  Enumerate vertices
        #for n in mesh.nodes():
            #ax.text(n.x(), n.y(), str(n.id()), color="C0")

    init_marker = j+2
    for j in range(y):
        for i in range(x):
            if u0[j][i]:
                mesh.createNode(float(i), float(j), 0., marker=init_marker)

    def uf(node):
        m = node.marker()
        if m == init_marker:
            pos = node.pos()
            x0 = int(pos.x())
            y0 = int(pos.y())
            return u0[y0][x0]
        return 0


    T = pg.solver.solveFiniteElements(mesh, a=c_dict, u0=uf, f=func, times=times)

    if show_solution:
        if axe:
            pg.viewer.mpl.drawField(axe, mesh, data=T[-1], nCols=20, shading='flat')
        else:
            ax, _ = pg.show(mesh, data=T[-1], label='Temperature $T$',
                cMap="hot_r", nCols=20, contourLines=False)
            pg.show(geom, ax=ax, fillRegion=False)
    return T[-1]

if __name__ == "__main__":
    f = Image.open('Images/Test_16.png').convert("L")
    f = numpy.asarray(f, dtype=int)
    f = f//255

    c = 100.
    t = 100.
    dt = 0.005
    N = int(t/dt)

    pBound = {'Node': [4230, 10]}
    c_dict= {0: c, 1: 0.00001, 2: 0.00001}
    times = numpy.linspace(0, t, N+1)

    u0 = numpy.zeros(f.shape)
    u0[37][182] = 10.

    img_diffusion_pg(f, u0, c_dict, times)

